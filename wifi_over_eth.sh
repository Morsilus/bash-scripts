#!/bin/bash


sudo ip link set up dev enp2s0
sudo ip addr add 192.168.1.1/24 dev enp2s0

sudo dhcpd

sudo iptables -t nat -A POSTROUTING -o wlp3s0 -j MASQUERADE
sudo iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A FORWARD -i enp2s0 -o wlp3s0 -j ACCEPT
