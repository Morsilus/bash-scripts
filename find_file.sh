#!/bin/bash

if [[ $# -eq 1 ]]; then
	find / 2>/dev/null | grep $1 | less
fi
